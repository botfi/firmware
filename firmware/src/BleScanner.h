

#pragma once
#include "Arduino.h"
#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEScan.h>
#include <BLEAdvertisedDevice.h>
#include <optional>

class BleScanner : public BLEAdvertisedDeviceCallbacks{
private:
    BLEUUID targetService;
    std::optional<BLEAdvertisedDevice> foundDevice;
    BLEScan* bleScan;

public:
    BleScanner(BLEUUID targetService): targetService(targetService){};
    int scanTime=5;
    void onResult(BLEAdvertisedDevice advertisedDevice) {
        if(advertisedDevice.haveServiceUUID() && advertisedDevice.getServiceUUID().equals(targetService)){
            Serial.printf("found target device : %s\n", advertisedDevice.toString().c_str());
            foundDevice = std::optional(advertisedDevice);
            bleScan->stop();
        } else {
            Serial.printf("Advertised Device: %s \n", advertisedDevice.toString().c_str());
        }
    }

    void setup() {
        bleScan = BLEDevice::getScan();
        bleScan->setAdvertisedDeviceCallbacks(this);
        bleScan->setActiveScan(true);
        bleScan->setInterval(100);
        bleScan->setWindow(99);
    }

    std::optional<BLEAdvertisedDevice> scan() {
        foundDevice.reset();
        BLEScanResults foundDevices = bleScan->start(scanTime, false);
        Serial.print("Devices found: ");
        Serial.println(foundDevices.getCount());
        Serial.println("Scan done!");
        bleScan->clearResults();
        return foundDevice;
    }
};
