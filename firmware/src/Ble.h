#pragma once


#include <optional>
#include "BLEDevice.h"
#include "Arduino.h"
#include "JoystickInput.h"
#include "CONSTANTS.h"

#define ARDUHAL_LOG_LEVEL ARDUHAL_LOG_LEVEL_VERBOSE

class Ble : public JoystickInput {
public:
    unsigned long lastTriggerChange = 0;
    boolean triggerStatus = 0;
private:
    signed char x = 0;
    signed char y = 0;
    BLEClient *client;
    BLERemoteService *remoteService;

    std::function<void(BLERemoteCharacteristic *, uint8_t *, size_t, bool)> bleNotifyCallback = [&](
            BLERemoteCharacteristic *pBLERemoteCharacteristic, uint8_t *pData, size_t length, bool isNotify) {
        if (length >= 3) {
            bool triggerPressed = pData[0] != 0;
            if(triggerStatus != triggerPressed){
                Serial.printf("trigger : %b \n",triggerStatus);
                lastTriggerChange = millis();
                triggerStatus = triggerPressed;
            }

            uint8_t directInputX = pData[1];
            uint8_t directInputY = pData[2];
            y = scale(-directInputY);
            x = scale(directInputX);
            //Serial.printf("ble input(l:%i) x:%i(%i)  y:%i(%i) \n", length, (signed char)directInputX, x, (signed char)directInputY, y);
            Serial.printf("ble input(l:%i)", length);
            for (int i = 0; i < length; i++) {
                Serial.printf(" %i:%i", i, (signed char) pData[i]);
            }
            Serial.printf("\n");

        } else {
            Serial.printf("ble input : %i", length);
        }
    };

public:
    signed char speed() override {
        return y;
    }

    signed char steer() override {
        return x;
    }

private:

    signed char scale(signed char p) {
        signed char borned = 0;
        if (p < -30) {
            borned = -30;
        } else if (p > 30) {
            borned = 30;
        } else {
            borned = p;
        }
        signed char r = map(borned, -30, 30, SIGNED_CHAR_MIN, SIGNED_CHAR_MAX);
        return r;
    }


public:
    Ble() {

    };

    void connect(BLEAddress deviceAddress, BLEUUID serviceUuid) {
        Serial.println("create client");
        client = BLEDevice::createClient();

        Serial.println("connect");
        client->connect(deviceAddress, BLE_ADDR_TYPE_RANDOM);
        Serial.print("Connection to server established : ");
        Serial.println(client->isConnected());


        remoteService = client->getService(serviceUuid);
        if (remoteService == nullptr) {
            Serial.print("Failed to find our service UUID: ");
            Serial.println(serviceUuid.toString().c_str());
        } else {
            Serial.println("Service found");
            std::map<std::string, BLERemoteCharacteristic *> *characteristics = remoteService->getCharacteristics();
            std::map<std::string, BLERemoteCharacteristic *>::iterator it;
            for (it = characteristics->begin(); it != characteristics->end(); it++) {
                Serial.print(it->first.c_str());
                Serial.print(":");
                Serial.println(it->second->toString().c_str());
                if (it->second->canNotify()) {
                    it->second->registerForNotify(bleNotifyCallback);
                }
            }
        }
    }

    void loop() {
        if (client->isConnected()) {
            std::map<std::string, BLERemoteCharacteristic *> *characteristics = remoteService->getCharacteristics();
            std::map<std::string, BLERemoteCharacteristic *>::iterator it;
            for (it = characteristics->begin(); it != characteristics->end(); it++) {
                if (it->second->canRead()) {
                    Serial.print(it->first.c_str());
                    Serial.print(" (value): ");
                    Serial.println(it->second->readUInt32());
                }
            }
        }
    }
};



